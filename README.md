Train Combinator
================

A program for finding all the ways to combine pieces of
a model train track together.

I'm pretty sure the algorithm is correct, but it is SUPER
inefficient. Especially given how large the number of
permutations becomes as you start increasing the pieces
of track in the inventory.
